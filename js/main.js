/* Search tieng viet khong dau
(function() {
    var _div = document.createElement('div');
    jQuery.fn.dataTable.ext.type.search.html = function(data) {
        _div.innerHTML = data;
        return _div.textContent ?
            _div.textContent
                .replace(/[àáãạảăắằẵặẳâấầẫậẩ]/g, 'a')
                // .replace(/[çÇ]/g, 'c')
                .replace(/[éèẽẹẻêếềễệể]/g, 'e')
                .replace(/[íìĩịỉ]/g, 'i')
                // .replace(/[ñÑ]/g, 'n')
                .replace(/[óòõọỏôốồỗộổơờớỡợở]/g, 'o')
                // .replace(/[ß]/g, 's')
                .replace(/[ùúũụủưừứữựử]/g, 'u') :
            _div.innerText.replace(/[àáãạảăắằẵặẳâấầẫậẩ]/g, 'a')
            // .replace(/[çÇ]/g, 'c')
                .replace(/[éèẽẹẻêếềễệể]/g, 'e')
                .replace(/[íìĩịỉ]/g, 'i')
                // .replace(/[ñÑ]/g, 'n')
                .replace(/[óòõọỏôốồỗộổơờớỡợở]/g, 'o')
                // .replace(/[ß]/g, 's')
                .replace(/[ùúũụủưừứữựử]/g, 'u');
    };
})();
*/
/*$(".hide-first").hide();*/


$(document).ready(function () {

    "use strict";
    // Activate tooltips
    /*$('[data-toggle="tooltip"]').tooltip();*/
    /*$("#input-search").sticky({topSpacing:0});*/

    /*$(document).ready(function(){
        $('#example1-tab1-dt').DataTable({
            columns: [
                { width: '20%' },
                { width: '20%' },
                { width: '20%' },
                { width: '10%' },
                { width: '15%' },
                { width: '15%' }
            ]
        });

        $('#example1-tab2-dt').DataTable({
            columns: [
                { width: '20%' },
                { width: '20%' },
                { width: '20%' },
                { width: '10%' },
                { width: '15%' },
                { width: '15%' }
            ]
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    });*/
    // Setup - add a text input to each footer cell
    /*$('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change clear', function () {
            if ( oTable.column(i).search() !== this.value ) {
                oTable
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );*/

    // $("table#example2 tr td[colspan=4]").attr('colspan',0);
    // $("table#example2 tr td[colspan=3]").attr('colspan',1);
    // $("table#example tr td[colspan=4]").attr('colspan',1);
    /*$(window).resize(function() {
        var width = $(window).width();
        if (width < 517){
            $("table#example tr td[colspan=4]").attr('colspan',3);
            alert("aaa")
        }
    });*/
/*    $(".hide-first").delay(4000).show(1000);
    $('.cd-intro').delay(4000).show().fadeOut('slow');

    $('.cd-intro').hide();
    $(".hide-first").show();*/

    // Setup - add a text input to each footer cell
    $('#example tfoot th').each( function () {
        var title = $(this).text();
        if(title=="STT"){
            $(this).html( '' );
        }else {
            $(this).html( '<input type="text" placeholder="Tìm '+title+'" />' );
        }

    } );
    $('#example2 tfoot th').each( function () {
        var title = $(this).text();
        if(title=="STT"){
            $(this).html( '' );
        }else {
            $(this).html( '<input type="text" placeholder="Tìm '+title+'" />' );
        }

    } );
    var dataSrc = [];
    var oTable = $('#example').DataTable( {
        "ajax": "Export.json",
        "deferRender": true,
        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            /*auto complete*/
            var api = this.api();

            // Populate a dataset for autocomplete functionality
            // using data from first, second and third columns
            api.cells('tr', [1,2,3,4,5]).every(function(){
                // Get cell data as plain text
                var data = $('<div>').html(this.data()).text();
                if(dataSrc.indexOf(data) === -1){ dataSrc.push(data); }
            });

            // Sort dataset alphabetically
            dataSrc.sort();

            // Initialize Typeahead plug-in
            $('.dataTables_filter input[type="search"]', api.table().container())
                .typeahead({
                        source: dataSrc,
                        afterSelect: function(value){
                            api.search(value).draw();
                        }
                    }
                );

        },
        "columnDefs": [
            { responsivePriority: 1, targets: 2 },
            { responsivePriority: 10001, targets: 2 },
            { responsivePriority: 2, targets: 1 },
            {
            "searchable": false,
            "orderable": true,
            // "targets": 0,
            // "type": "html"
            // "targets": '_all'
        }
       /* { responsivePriority: 1, targets: 2 },
        { responsivePriority: 10001, targets: 2 },
        { responsivePriority: 2, targets: 1 }*/
        ],
        /*columns: [
            { width: '5%' },
            { width: '8%' },
            { width: '15%' },
            { width: '15%' },
            { width: '15%' },
            { width: '43%' }
        ],*/
        "sDom": '<"view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"view-pager"<"col-sm-12"<"text-center"ip>>>',
        "lengthChange": true,
        searching: true,
        info: false,
        "pageLength": 100,
        "order": [],
        orderCellsTop: true,
        mark: true,
        fixedHeader: {
            header: true,
            footer: false
        },
        responsive: true,
        drawCallback: function ( settings ) {
            var groupColumn = 1;
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var colspan = 6;
            if ($(window).width() >= 510) {
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="dtrg-group dtrg-start dtrg-level-0"><td colspan="'+colspan+'"><div class="alert alert-primary text-uppercase mb-0" role="alert" ><span class="text-white badge badge-pill badge-danger font-weight-normal" style="font-size:1.3em ">《 '+group.replace(/<[^>]+>/g, '')+' 》</span><span class="text-capitalize">【Từ điển Từ Vựng Minano nihongo được biên soạn bởi đội ngũ <span class="text-danger">Tiếng Nhật Đơn Giản</span>】</span></div></td></tr>'
                        );

                        last = group;
                    }
                } );
            }

        },
        /*rowGroup: {
            startRender: function (rows,group ) {
                return '<div class="alert alert-primary text-uppercase mb-0" role="alert" ><span class="text-white badge badge-pill badge-danger font-weight-normal" style="font-size:1.3em ">《 '+group.replace(/<[^>]+>/g, '')+' 》</span><span class="text-capitalize">【Từ điển Từ Vựng Minano nihongo được biên soạn bởi đội ngũ <span class="text-danger">Tiếng Nhật Đơn Giản</span>】</span></div>';            },
            dataSrc: 1
        },*/
        "language": {
            "lengthMenu": 'Hiển thị　<select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="-1">Tất cả</option></select>　Từ vựng',
            /*"search": "Tìm kiếm:　",*/
            "search": "",
            "searchPlaceholder": "Nhập nghĩa Tiếng Việt hoặc Tiếng Nhật",
            "zeroRecords": "Không tìm thấy nội dung!",
            "infoEmpty": "Không có dữ liệu",
            "paginate": {
                "previous": "Trang trước",
                "next": "Trang sau"
            }
        },
        'createdRow': function(row, data, dataIndex){
            // Use empty value in the "Office" column
            // as an indication that grouping with COLSPAN is needed
            if ($(window).width() >= 510) {
                if(data[2] !== '' && data[3] === ''){
                    // Add COLSPAN attribute
                    let cp = 3;
                    if(data[5] ==='') { cp = 4; $('td:eq(4)', row).css('display', 'none');$('td:eq(5)', row).css('display', 'none');}
                    $('td:eq(2)', row).attr('colspan', cp);

                    // Hide required number of columns
                    // next to the cell with COLSPAN attribute
                    //    $('td:eq(3)', row).css('display', 'none');
                    $('td:eq(3)', row).css('display', 'none');
                    $('td:eq(4)', row).css('display', 'none');

                }
            }
         }

    } );
    $('div.dataTables_filter input').addClass('text-center form-control global_filter');
    $('div.dataTables_filter input').removeClass('form-control-sm');
    $('div.dataTables_filter').prependTo( ".form-search");
    /*$('#example2').DataTable().search( 'Bài 2' ).draw();*/

    // go tieng viet : Remove accented character from search input as well
       /* $('#example_filter input[type=search]').keyup( function () {
        oTable.search(
            jQuery.fn.DataTable.ext.type.search.html(this.value)
        ).draw();
    } );*/

    /*Them STT*/
    oTable.on( 'order.dt search.dt', function () {
        oTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    var window_width = $(window).width(),
        window_height = window.innerHeight,
        header_height = $(".default-header").height(),
        header_height_static = $(".site-header.static").outerHeight(),
        fitscreen = window_height - header_height;


    $(".fullscreen").css("height", window_height)
    $(".fitscreen").css("height", fitscreen);

    /*$('.search-btn').on( 'click', function () {
        var search_value =  $('#example_filter input[type=search]').val();
        oTable.search( search_value ).draw();
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".main-table").offset().top - 20
        }, 500);
    } );*/

    /*Scroll when focus search button*/
    $("#example_filter input[type=search]").focus(function(){
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#example_filter input[type=search]").offset().top - 100
        }, 500);
    });
    /*$('#example_filter input[type=search]').on( 'keyup focus', function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#example_filter input[type=search]").offset().top - 20
        }, 500);
    } );*/

    /*Su kien cho nut search*/
    $(".courses-search a").click(function(){
        // var regex = '' + this.text + '';
        oTable.search(this.text, true, false).draw();
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#example_filter input[type=search]").offset().top - 20
        }, 500);
    });
    /*Render button bai viet*/
    setTimeout(function(){
        oTable.column(1).data().unique().each( function (item, i) {
            var number = 0, text="";
            if ($(window).width() > 768) {
                number=24;
                text="　Xem tiếp từ vựng N4　";
            }
            else if (($(window).width() < 768) && ($(window).width() > 510)){
                number = 6;
                text="　Xem tiếp từ vựng　";
            }
            else {
                number = 4;
                text="　Xem tiếp từ vựng　";
            }
            if(i==number){
                $(".courses-search").append(
                    '<p class="mb-0"><button class="btn btn-info" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> <i class="fa fa-hand-o-down text-warning" aria-hidden="true"></i>　'+text+'　<i class="fa fa-hand-o-down text-warning" aria-hidden="true"></i></button></p><div class="collapse n4-wrap" id="collapseExample"><hr></div>');
            }
            else if(i>number){
                $(".n4-wrap").append(
                    $('<a class="primary-btn transparent mr-10 mb-10 text-white" style="transition: 0.3s all ease-in" >'+item.replace(/(<([^>]+)>)/gi, "")+'</a>')
                        .on( 'click', function () {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(".courses.courses-search").offset().top - 80
                            }, 500);
                            $(this).toggleClass('active');
                            var items = $(".courses-search").find('.active').map( function () {
                                return $(this).text();
                            } ).toArray().join('|');
                            if(items){
                                oTable.column(1).search( "(^"+items+"$)",true,false ).draw();
                            }
                            else {
                                oTable.column(1).search( items, true, false ).draw();
                            }
                        } )
                );
            }
            else {
                $(".courses-search").append(
                    $('<a class="primary-btn transparent mr-10 mb-10 text-white" style="transition: 0.3s all ease-in" >'+item.replace(/(<([^>]+)>)/gi, "")+'</a>')
                        .on( 'click', function () {
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(".courses.courses-search").offset().top - 80
                            }, 500);
                            $(this).toggleClass('active');
                            var items = $(".courses-search").find('.active').map( function () {
                                return $(this).text();
                            } ).toArray().join('|');
                            if(items){
                                oTable.column(1).search( "(^"+items+"$)",true,false ).draw();
                            }
                            else {
                                oTable.column(1).search( items, true, false ).draw();
                            }
                        } )
                );
            }

        } );
    }, 500);
    $("table tr td[colspan=4]").attr('colspan',2);
    // Wow Js
    /*new WOW().init();


    if (document.getElementById("default-select")) {
        $('select').niceSelect();
    };*/

    /*$('.img-pop-up').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });


    $('.play-btn').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });*/

    /* ---------------------------------------------
        scroll body to 0px on click
     --------------------------------------------- */
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });


    //  Counter Js

    /*$('.counter').counterUp({
        delay: 10,
        time: 1000
    });*/


    // Initiate superfish on nav menu
    $('.nav-menu').superfish({
        animation: {
            opacity: 'show'
        },
        speed: 400
    });

    // Mobile Navigation
    if ($('#nav-menu-container').length) {
        var $mobile_nav = $('#nav-menu-container').clone().prop({
            id: 'mobile-nav'
        });
        $mobile_nav.find('> ul').attr({
            'class': '',
            'id': ''
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="lnr lnr-menu"></i></button>');
        $('body').append('<div id="mobile-body-overly"></div>');
        $('#mobile-nav').find('.menu-has-children').prepend('<i class="lnr lnr-chevron-down"></i>');

        $(document).on('click', '.menu-has-children i', function (e) {
            $(this).next().toggleClass('menu-item-active');
            $(this).nextAll('ul').eq(0).slideToggle();
            $(this).toggleClass("lnr-chevron-up lnr-chevron-down");
        });

        $(document).on('click', '#mobile-nav-toggle', function (e) {
            $('body').toggleClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('lnr-cross lnr-menu');
            $('#mobile-body-overly').toggle();
        });

        $(document).click(function (e) {
            var container = $("#mobile-nav, #mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('lnr-cross lnr-menu');
                    $('#mobile-body-overly').fadeOut();
                }
            }
        });
    } /*else if ($("#mobile-nav, #mobile-nav-toggle").length) {
        $("#mobile-nav, #mobile-nav-toggle").hide();
    }*/

    // Smooth scroll for the menu and links with .scrollto classes
    $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            if (target.length) {
                var top_space = 0;

                if ($('#header').length) {
                    top_space = $('#header').outerHeight();

                    if (!$('#header').hasClass('header-fixed')) {
                        top_space = top_space;
                    }
                }

                $('html, body').animate({
                    scrollTop: target.offset().top - top_space
                }, 1500, 'easeInOutExpo');

                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('lnr-times lnr-bars');
                    $('#mobile-body-overly').fadeOut();
                }
                return false;
            }
        }
    });




    // Header scroll class
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#header').addClass('header-scrolled');
            $('#logo').css("opacity", "1");
            $('#logo img').addClass('header-logo-scroll');
            $('#back-top').fadeIn(500);
            $('.nav-title').removeClass('d-none');


        } else {
            $('#header').removeClass('header-scrolled');
            $('#logo img').removeClass('header-logo-scroll');
            $('#logo').css("opacity", ".8");
            $('#back-top').fadeOut(500);
            $('.nav-title').addClass('d-none');
        }
    });


    // Rocket Scroll
    $(window).scroll(function () {
        if ($(this).scrollTop() > 10) {
            $('.rocket-img').addClass('go-top');
            $('#back-top').fadeIn(500);
        } else {
            $('.rocket-img').removeClass('go-top');
            $('#back-top').fadeOut(500);
        }
    });


    // Owl Carousel
    /*if ($('.testi-slider').length) {
        $('.testi-slider').owlCarousel({
            loop: true,
            margin: 30,
            items: 1,
            nav: false,
            autoplay: 2500,
            smartSpeed: 1500,
            dots: true,
            responsiveClass: true,
            thumbs: true,
            thumbsPrerendered: true,
            navText: ["<i class='lnr lnr-arrow-left'></i>", "<i class='lnr lnr-arrow-right'></i>"]
        })
    }*/


    //  Start Google map

    // When the window has finished loading create our google map below

    // if (document.getElementById("map")) {
    //
    //     google.maps.event.addDomListener(window, 'load', init);
    //
    //     function init() {
    //         // Basic options for a simple Google Map
    //         // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    //         var mapOptions = {
    //             // How zoomed in you want the map to start at (always required)
    //             zoom: 11,
    //
    //             // The latitude and longitude to center the map (always required)
    //             center: new google.maps.LatLng(40.6700, -73.9400), // New York
    //
    //             // How you would like to style the map.
    //             // This is where you would paste any style found on Snazzy Maps.
    //             styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
    //         };
    //
    //         // Get the HTML DOM element that will contain your map
    //         // We are using a div with id="map" seen below in the <body>
    //         var mapElement = document.getElementById('map');
    //
    //         // Create the Google Map using our element and options defined above
    //         var map = new google.maps.Map(mapElement, mapOptions);
    //
    //         // Let's also add a marker while we're at it
    //         var marker = new google.maps.Marker({
    //             position: new google.maps.LatLng(40.6700, -73.9400),
    //             map: map,
    //             title: 'Snazzy!'
    //         });
    //     }
    // }

    /*$('#mc_embed_signup').find('form').ajaxChimp();*/
});

